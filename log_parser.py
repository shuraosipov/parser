from collections import Counter
import collections
import datetime
import math
import sys
import re


def calc_percentile(data, quartile=95):
    # A percentile (item in a data set) is defined as a point below
    # which a certain percent of the observations (elements in data set) lie.
    #
    # Good explanation - https://www.youtube.com/watch?v=uwSfZxwuoyw
    #
    # Calculate percentile using the following formula:
    #           p = (quartile / 100) * (n + 1)
    # where:
    # quartile - quartile to be found (in range 1 - 99), by default quartile value (in this script) is 95 %
    # n - number of elements in a data set
    # p - position of the element in a data set that points us to a percentile

    data_set = sorted(data)
    n = len(data_set)
    p = (quartile / 100) * (n + 1)

    # here discovered 'p' would be rounded up and down as we need to use these values to find percentile position
    above = math.ceil(p)
    below = math.floor(p)

    if n == 1:
        return data_set[0]
    elif above == below:
        return data_set[p]
    else:
        # if rounded values are equal then return 'p' element in the data set
        percentile = (data_set[above] + data_set[below]) / 2
        return percentile


def parser(input_file, output_file='output.txt'):
    # Temporary dicts and list for processing
    temp = {}
    tmp1 = {}  # very creative
    results = {}
    request_time = {}
    total_time = []

    with open(input_file) as log:
        for row_line in log:
            line = row_line.split()
            if len(line) == 5:
                req, action, group, backend = line[1], line[2], line[3], line[4]
            elif len(line) == 4:
                req, action, group = line[1], line[2], line[3]

            # Task 3 calculation
            if 'BackendConnect' in line and req not in temp:
                temp[req] = {group: {'action': action, 'backend': backend, 'error': []}}

            elif 'BackendConnect' in line and req in temp and group not in temp[req]:
                temp[req][group] = {'action': action, 'backend': backend, 'error': []}

            elif 'BackendError' in line:
                req, action, group, message = line[1], line[2], line[3], line[4]
                backend = temp[req][group]['backend']

                if group not in results:
                    results[group] = {backend: {'request': [req], 'error': [" ".join(line[4:])]}}
                elif group in results and backend not in results[group]:
                    results[group][backend] = {'request': [req], 'error': [" ".join(line[4:])]}
                elif group in results and backend in results[group] and req not in results[group][backend]:
                    results[group][backend]['request'].append(req)
                    results[group][backend]['error'].append(" ".join(line[4:]))
                del temp[req][group]

            elif 'BackendOk' in line:
                req, action, group = line[1], line[2], line[3]
                backend = temp[req][group]['backend']

                if group not in results:
                    results[group] = {backend: {'request': [req], 'error': [action]}}
                elif group in results and backend not in results[group]:
                    results[group][backend] = {'request': [req], 'error': [action]}
                elif group in results and backend in results[group] and req not in results[group][backend]:
                    results[group][backend]['request'].append(req)
                    results[group][backend]['error'].append(action)
                del temp[req][group]

            elif 'StartRequest' in line:
                time, req = line[0], line[1]
                tmp1[req] = {'start_request': time, 'send_results': '', 'finish_request': ''}

            elif 'StartSendResult' in line:
                time, req = line[0], line[1]
                tmp1[req]['send_results'] = time

            elif 'FinishRequest' in line:
                time, req = line[0], line[1]
                tmp1[req]['finish_request'] = time

        # Task 4 calculation
        # The rest in temp indicates requests without any response from backend (nor BackendOk or BackendError)
        counter = 0
        for req in temp:
            if len(temp[req]) > 0:
                for group in temp[req]:
                    backend = temp[req][group]['backend']

                    if group not in results:
                        results[group] = {backend: {'request': [req], 'error': ['No response']}}
                    elif group in results and backend not in results[group]:
                        results[group][backend] = {'request': [req], 'error': ['No response']}
                    elif group in results and backend in results[group] and req not in results[group][backend]:
                        results[group][backend]['request'].append(req)
                        results[group][backend]['error'].append('No response')
                    counter += 1
                del temp[req][group]

        # For 2 calculation
        # Find request id(s) with longest response time and total time for processing each request
        for req in tmp1:
            send_results = int((tmp1[req]['send_results']))
            finish_request = int((tmp1[req]['finish_request']))
            start_request = int((tmp1[req]['start_request']))
            start = datetime.datetime.utcfromtimestamp(send_results * 0.000001)
            end = datetime.datetime.utcfromtimestamp(finish_request * 0.000001)
            init = datetime.datetime.utcfromtimestamp(start_request * 0.000001)
            send_back_results_time = (end - start).total_seconds()
            request_processing_time = (end - init).total_seconds()
            request_time[req] = send_back_results_time
            total_time.append(request_processing_time)

    # Write results to a file
    with open(output_file, mode='w') as out:

        # Task 1 results - 95th percentile of the request processing time (in seconds)
        print('95th percentile of the request processing time (in seconds):', calc_percentile(total_time), file=out)

        # Task 2 results - Top 10 longest requests
        top_ten = (dict(Counter(request_time).most_common(10)))
        print('Request id(s) with longest reponse time:', ', '.join([str(k) for k in top_ten]), file=out)

        # Task 4 results - Number of requests without response from backend
        print('Number of requests with incomplete data from backend:', counter, file=out)

        # Task 3 results - backend statistics
        # Natural sorting in dict
        od = collections.OrderedDict(sorted(results.items(), key=lambda t: int(t[0])))
        print('', file=out)
        print('Requests and backend errors:', file=out)
        for group in od:
            print('Replica group', group, ':', file=out)

            for backend in results[group]:
                # beautify backend name using regexp
                pattern = re.compile(r'//(.*)/')
                beautiful_backend = "".join(pattern.findall(backend))
                print('    ', beautiful_backend, file=out)
                requests = results[group][backend]['request']
                errors = results[group][backend]['error']

                print('     Requests:', len(requests), file=out)
                print('     Errors:', file=out)
                for error in set(errors):
                    if len(set(errors)) > 1 and 'BackendOk' not in set(errors):
                        count = errors.count(error)
                        print('         ', error, ':', count, file=out)


if __name__ == "__main__":
    if len(sys.argv) > 1:
        file = sys.argv[1]
        parser(file)
    else:
        print('No input file specified')
        sys.exit(1)
